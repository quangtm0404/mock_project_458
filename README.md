# FA Training Management System
- API for Mocking project build LMS system for FA
- Using `ASP .Net Core (Net 7)`
- Unit Testing: Using `XUnit` to test and `Moq` to create mocking object test coverage (65%)
- Apply CI using `Docker`

## This project is a cloned project of GroupWork `458 group` through our intership (Jan 2023 - Apr 2023)